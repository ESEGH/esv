/* 
 * File:   gpio.h
 * Author: hadyk
 *
 * Created on May 6, 2018, 8:27 PM
 */

#ifndef GPIO_H
#define	GPIO_H

#ifdef	__cplusplus
extern "C" {
#endif

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0
    
// get/set SDI1 aliases
#define SDI1_TRIS                 TRISBbits.TRISB4
#define SDI1_LAT                  LATBbits.LATB4
#define SDI1_PORT                 PORTBbits.RB4
#define SDI1_WPU                  WPUBbits.WPUB4
#define SDI1_ANS                  ANSELHbits.ANS10
#define SDI1_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define SDI1_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define SDI1_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define SDI1_GetValue()           PORTBbits.RB4
#define SDI1_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define SDI1_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define SDI1_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define SDI1_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define SDI1_SetAnalogMode()      do { ANSELHbits.ANS10 = 1; } while(0)
#define SDI1_SetDigitalMode()     do { ANSELHbits.ANS10 = 0; } while(0)

// get/set SCK1 aliases
#define SCK1_TRIS                 TRISBbits.TRISB6
#define SCK1_LAT                  LATBbits.LATB6
#define SCK1_PORT                 PORTBbits.RB6
#define SCK1_WPU                  WPUBbits.WPUB6
#define SCK1_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define SCK1_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define SCK1_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define SCK1_GetValue()           PORTBbits.RB6
#define SCK1_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define SCK1_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define SCK1_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define SCK1_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)

// get/set CS3 aliases
#define CS3_TRIS                 TRISCbits.TRISC4
#define CS3_LAT                  LATCbits.LATC4
#define CS3_PORT                 PORTCbits.RC4
#define CS3_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define CS3_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define CS3_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define CS3_GetValue()           PORTCbits.RC4
#define CS3_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define CS3_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)

// get/set CS2 aliases
#define CS2_TRIS                 TRISCbits.TRISC5
#define CS2_LAT                  LATCbits.LATC5
#define CS2_PORT                 PORTCbits.RC5
#define CS2_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define CS2_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define CS2_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define CS2_GetValue()           PORTCbits.RC5
#define CS2_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define CS2_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)

// get/set CS1 aliases
#define CS1_TRIS                 TRISCbits.TRISC6
#define CS1_LAT                  LATCbits.LATC6
#define CS1_PORT                 PORTCbits.RC6
#define CS1_ANS                  ANSELHbits.ANS8
#define CS1_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define CS1_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define CS1_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define CS1_GetValue()           PORTCbits.RC6
#define CS1_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define CS1_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define CS1_SetAnalogMode()      do { ANSELHbits.ANS8 = 1; } while(0)
#define CS1_SetDigitalMode()     do { ANSELHbits.ANS8 = 0; } while(0)

// get/set SDO1 aliases
#define SDO1_TRIS                 TRISCbits.TRISC7
#define SDO1_LAT                  LATCbits.LATC7
#define SDO1_PORT                 PORTCbits.RC7
#define SDO1_ANS                  ANSELHbits.ANS9
#define SDO1_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define SDO1_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define SDO1_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define SDO1_GetValue()           PORTCbits.RC7
#define SDO1_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define SDO1_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define SDO1_SetAnalogMode()      do { ANSELHbits.ANS9 = 1; } while(0)
#define SDO1_SetDigitalMode()     do { ANSELHbits.ANS9 = 0; } while(0)


#ifdef	__cplusplus
}
#endif

#endif	/* GPIO_H */

