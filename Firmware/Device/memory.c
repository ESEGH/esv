#include "memory.h"

void EEPROM_Write(unsigned char address,unsigned char data )
{
  unsigned char GIEBitValue = INTCONbits.GIE;     // Save interrupt enable
  EEADR = address;
  EEDATA = data;
  EECON1bits.EEPGD = 0;   // Point to EEPROM Memory
  EECON1bits.CFGS = 0;    // Access to EEPROM
  EECON1bits.WREN = 1;    // Enable Write
  INTCONbits.GIE = 0;     // Disable interrupts
  EECON2 = 0x55;
  EECON2 = 0xAA;
  EECON1bits.WR = 1;      // Begin Write
  INTCONbits.GIE = GIEBitValue;   // Restore interrupt enable

  while(PIR2bits.EEIF == 0);  // Wait for Write to get Complete
  PIR2bits.EEIF = 0;      // Clear Flag
  EECON1bits.WREN = 0;    // Disable Write
}

unsigned char EEPROM_Read( unsigned char address )
{
  unsigned char data;
  EEADR = address;
  EECON1bits.EEPGD = 0;   // Point to EEPROM
  EECON1bits.CFGS = 0;    // Access to EEPROM
  EECON1bits.RD = 1;      // Enable Read
  data = EEDATA;
  return data;
}
