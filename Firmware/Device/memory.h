#ifndef MEMORY_H
#define MEMORY_H

#include "HardwareProfile.h"
#include "Compiler.h"

void EEPROM_Write(unsigned char address,unsigned char data );
unsigned char EEPROM_Read( unsigned char address );


#endif