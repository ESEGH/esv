#-------------------------------------------------
#
# Project created by QtCreator 2018-04-21T15:12:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets serialport

TARGET = ESV-GUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    hid_pnp.cpp \
    Comm.cpp \
    Device.cpp \
    DeviceData.cpp \
    ImportExportHex.cpp \
    Settings.cpp \
    UpdateWindow.cpp

HEADERS  += mainwindow.h \
    hid_pnp.h \
    Comm.h \
    Device.h \
    DeviceData.h \
    ImportExportHex.h \
    Settings.h \
    UpdateWindow.h \
    ../../version.h

FORMS    += mainwindow.ui \
    Settings.ui \
    UpdateWindow.ui

RESOURCES += \
    esv-resource.qrc

LIBS += -L../HIDAPI_BL/release -L../HIDAPI_DEV/release -lHIDAPI_BL -lHIDAPI_DEV
LIBS += -lSetupAPI

OBJECTS_DIR = obj
MOC_DIR     = moc
