#include "hid_pnp.h"

enum
{
  CMD_TOGGLE_LED = 0x80,
  CMD_READ_LEVELS = 0x55,
  CMD_WRITE_LEVELS = 0x65,
  CMD_READ_POT = 0x37,
  CMD_RESET_2BL = 0x45,
  CMD_NOP = 0x00
};

HID_PnP::HID_PnP(QObject *parent) : QObject(parent)
{
   open_device();
}

HID_PnP::~HID_PnP()
{
    disconnect(timer, SIGNAL(timeout()), this, SLOT(PollUSB()));
}

void HID_PnP::open_device()
{
  isConnected = false;
  potentiometerValue = 0;
  value_channel_1 = 127;
  value_channel_2 = 127;
  value_channel_3 = 127;
  version_ = 0;

  isConnected_old = isConnected,
  value_channel_1_old = value_channel_1,
  value_channel_2_old = value_channel_2,
  value_channel_3_old = value_channel_3,
  potentiometerValue_old = potentiometerValue;

  readLevels = true;
  writeLevels = 0;
  reset_to_bl_ = false;

  version_old_ = version_;

  device = NULL;
  memset((void*)rcbuf, 0x00, sizeof(rcbuf));
  memset((void*)txbuf, 0x00, sizeof(txbuf));

  timer = new QTimer();
  connect(timer, SIGNAL(timeout()), this, SLOT(PollUSB()));

  timer->start(DEFAULT_POLLUSB_TIMEOUT);
}

void HID_PnP::close_device()
{
  timer->stop();
  hid_close(device);
  device = NULL;

  isConnected = false;
  readLevels = true;
  potentiometerValue = 0;
  value_channel_1 = 127;
  value_channel_2 = 127;
  value_channel_3 = 127;

  version_ = 0;

  hid_comm_update(version_, isConnected, value_channel_1, value_channel_2, value_channel_3, potentiometerValue);
}

void HID_PnP::update_channels(int v1, int v2, int v3)
{
  value_channel_1 = v1;
  value_channel_2 = v2;
  value_channel_3 = v3;

  writeLevels = true;

}

void HID_PnP::PollUSB()
{
    memset((void*)rcbuf, 0x00, sizeof(rcbuf));
    memset((void*)txbuf, 0x00, sizeof(txbuf));

    if (isConnected == false)
    {
        device = hid_open(0x04d8, 0x003f, NULL);

        if (device)
        {
            isConnected = true;
            readLevels = true;
            hid_set_nonblocking(device, true);
            timer->start(DEFAULT_POLLUSB_TIMEOUT);
        }
    }
    else
    {

      txbuf[1] = CMD_TOGGLE_LED;

      if(readLevels)
      {
        txbuf[2] = CMD_READ_LEVELS;
        readLevels = false;
      }
      else
      {
        txbuf[2] = CMD_NOP;
      }

      if(writeLevels)
      {
        txbuf[3] = CMD_WRITE_LEVELS;
        writeLevels = false;
      }
      else
      {
        txbuf[3] = CMD_NOP;
      }

      txbuf[4] = value_channel_1;
      txbuf[5] = value_channel_2;
      txbuf[6] = value_channel_3;

      if(reset_to_bl_)
      {
        reset_to_bl_ = false;
        txbuf[7] = CMD_RESET_2BL;
      }
      else
        txbuf[7] = CMD_NOP;

      if (hid_write(device, txbuf, sizeof(txbuf)) == -1)
      {
        CloseDevice();
        return;
      }

      if(hid_read(device, rcbuf, sizeof(rcbuf)) == -1)
      {
        CloseDevice();
        return;
      }

      if(rcbuf[0] == CMD_READ_LEVELS)
      {
        value_channel_1 = rcbuf[1];
        value_channel_2 = rcbuf[2];
        value_channel_3 = rcbuf[3];
      }

      potentiometerValue = (rcbuf[5] << 8) + rcbuf[4];

      version_ = rcbuf[6] << 8;
      version_ |= rcbuf[7];

    }

    if(isConnected != isConnected_old ||
       value_channel_1 != value_channel_1_old ||
       value_channel_2 != value_channel_2_old ||
       value_channel_3 != value_channel_3_old ||
       potentiometerValue != potentiometerValue_old ||
       version_ != version_old_ )
    {
      isConnected_old = isConnected,
      value_channel_1_old = value_channel_1,
      value_channel_2_old = value_channel_2,
      value_channel_3_old = value_channel_3,
      potentiometerValue_old = potentiometerValue;
      version_old_ = version_;


      hid_comm_update(version_,isConnected, value_channel_1, value_channel_2, value_channel_3, potentiometerValue);
    }

    timer->start(DEFAULT_POLLUSB_TIMEOUT);
}

void HID_PnP::read_levels()
{
  readLevels = true;
}

void HID_PnP::reset_to_bl()
{
  reset_to_bl_ = true;
}

void HID_PnP::CloseDevice()
{
    hid_close(device);
    device = NULL;

    isConnected = false;

    potentiometerValue = 0;
    value_channel_1 = 127;
    value_channel_2 = 127;
    value_channel_3 = 127;

    version_ = 0;

    hid_comm_update(version_,isConnected, value_channel_1, value_channel_2, value_channel_3, potentiometerValue);
    timer->start(DEFAULT_POLLUSB_TIMEOUT);
}
