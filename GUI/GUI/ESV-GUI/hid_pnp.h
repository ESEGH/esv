#ifndef HID_PNP_H
#define HID_PNP_H

#include <QObject>
#include <QTimer>
#include "../HIDAPI_DEV/hidapi_dev.h"

#include <wchar.h>
#include <string.h>
#include <stdlib.h>

#define MAX_STR 65
#define DEFAULT_POLLUSB_TIMEOUT 500

class HID_PnP : public QObject
{
    Q_OBJECT
public:
    explicit HID_PnP(QObject *parent = 0);
    ~HID_PnP();

  void open_device();
  void close_device();

signals:

    void hid_comm_update(int version, bool isConnected, int chv1, int chv2, int chv3, int potentiometerValue);

public slots:

    void read_levels();
    void PollUSB();
    void update_channels(int v1, int v2, int v3);
    void reset_to_bl();

private:

    bool
      isConnected,
    isConnected_old,
      readLevels,
      reset_to_bl_,
      writeLevels;

    int
      potentiometerValue,
      value_channel_1,
      value_channel_2,
      value_channel_3,
      value_channel_1_old,
      value_channel_2_old,
      value_channel_3_old,
      potentiometerValue_old,
      version_,
      version_old_;

    hid_device
      *device;

    QTimer
      *timer;

    unsigned char rcbuf[MAX_STR];
    unsigned char txbuf[MAX_STR];

   void CloseDevice();

};

#endif // HID_PNP_H
