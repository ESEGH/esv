#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../../version.h"


MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  ui->pbFirmwareUpdate->setVisible(true);

  setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);

  lblConStatus = new QLabel(TXT_NOT_CONNECTED);
  lblFirmwareVersion = new QLabel(QString("Firmware version --"));
    ui->statusBar->addPermanentWidget(lblFirmwareVersion);
  ui->statusBar->addPermanentWidget(lblConStatus);


  level_values_[0] = DEFAULT_LEVEL_VALUE;
  level_values_[1] = DEFAULT_LEVEL_VALUE;
  level_values_[2] = DEFAULT_LEVEL_VALUE;

  set_temperature(0);

  plugNPlay = new HID_PnP();

  connect(this, SIGNAL(update_channels_hid(int,int,int)), plugNPlay, SLOT(update_channels(int,int,int)));
  connect(this, SIGNAL(read_levels()), plugNPlay, SLOT(read_levels()));
  connect(plugNPlay, SIGNAL(hid_comm_update(int, bool, int, int, int, int)), this, SLOT(hid_comm_update(int, bool, int, int, int, int)));
  connect(this, SIGNAL(reset_to_bl()), plugNPlay, SLOT(reset_to_bl()));

  setWindowTitle(QString("%1 Version: %2").arg(APPLICATION).arg(VERSION));
}

MainWindow::~MainWindow()
{
  disconnect(plugNPlay, SIGNAL(hid_comm_update(bool, int, int, int, int)), this, SLOT(hid_comm_update(bool, int, int, int, int)));

  delete ui;
  delete plugNPlay;
}

void MainWindow::hid_comm_update(int version, bool isConnected, int chv1, int chv2, int chv3, int potentiometerValue)
{
  set_temperature(potentiometerValue);
  level_values_[CHANNEL_1] = chv1;
  level_values_[CHANNEL_2] = chv2;
  level_values_[CHANNEL_3] = chv3;

  update_chanels(level_values_);

  if(!isConnected)
  {
    lblConStatus->setText(TXT_DISCONNECTED);
    enable_widgets(false);
  }
  else
  {
    lblConStatus->setText(TXT_CONNECTED);
    enable_widgets(true);
  }

  lblFirmwareVersion->setText(QString("Firmware version %1.%2.%3").arg(version/100).arg((version%100)/10).arg(((version%100)%10)));
}

void MainWindow::enable_widgets(bool enable)
{
  ui->gpChannel1->setEnabled(enable);
  ui->gpChannel2->setEnabled(enable);
  ui->gpChannel3->setEnabled(enable);

  ui->pbDefault->setEnabled(enable);
  //ui->pbFirmwareUpdate->setEnabled(enable);
  ui->gpTemerature->setEnabled(enable);

}

void MainWindow::update_chanels(uint8_t *values)
{
  ui->leCh1Value->setText(QString("%1").arg(values[CHANNEL_1]));
  ui->leCh2Value->setText(QString("%1").arg(values[CHANNEL_2]));
  ui->leCh3Value->setText(QString("%1").arg(values[CHANNEL_3]));
}

void MainWindow::on_pbDefault_clicked()
{
  level_values_[CHANNEL_1] = DEFAULT_LEVEL_VALUE;
  level_values_[CHANNEL_2] = DEFAULT_LEVEL_VALUE;
  level_values_[CHANNEL_3] = DEFAULT_LEVEL_VALUE;

  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);

}

void MainWindow::set_temperature( double temp  )
{
  ui->leTemperature->setText(QString("%1").arg(temp));
}

void MainWindow::increment_level(uint8_t no )
{
  if(level_values_[no] == 255)
    return;

  level_values_[no]++;

}

void MainWindow::decrement_level(uint8_t no )
{
  if(level_values_[no] == 0)
    return;

  level_values_[no]--;

}

void MainWindow::on_tbCh1Up_clicked()
{
  increment_level(CHANNEL_1);
  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);
}

void MainWindow::on_tbCh1Down_clicked()
{
  decrement_level(CHANNEL_1);
  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);
}

void MainWindow::on_tbCh2Up_clicked()
{
  increment_level(CHANNEL_2);
  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);
}

void MainWindow::on_tbCh2Down_clicked()
{
  decrement_level(CHANNEL_2);
  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);
}

void MainWindow::on_tbCh3Up_clicked()
{
  increment_level(CHANNEL_3);
  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);
}

void MainWindow::on_tbCh3Down_clicked()
{
  decrement_level(CHANNEL_3);
  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);
}

void MainWindow::on_pbFirmwareUpdate_clicked()
{
  updtform_ = new UpdateWindow(this);
  updtform_->setWindowModality(Qt::ApplicationModal);
  updtform_->show();

  emit reset_to_bl();

  //delete updtform_;
}

void MainWindow::on_pbRead_clicked()
{
  emit read_levels();
}

void MainWindow::on_leCh1Value_editingFinished()
{
  level_values_[CHANNEL_1] = ui->leCh1Value->text().toInt();
  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);
}

void MainWindow::on_leCh2Value_editingFinished()
{
  level_values_[CHANNEL_2] = ui->leCh2Value->text().toInt();
  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);
}

void MainWindow::on_leCh3Value_editingFinished()
{
  level_values_[CHANNEL_3] = ui->leCh3Value->text().toInt();
  update_chanels(level_values_);
  emit update_channels_hid(level_values_[CHANNEL_1],level_values_[CHANNEL_2],level_values_[CHANNEL_3]);
}
