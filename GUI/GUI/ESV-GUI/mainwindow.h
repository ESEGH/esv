#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QComboBox>
#include <QLabel>
#include <QToolButton>
#include <QPushButton>
#include <QComboBox>
#include <QWindow>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QMessageBox>
#include <stdint.h>
#include "UpdateWindow.h"
#include "hid_pnp.h"

#define DEFAULT_LEVEL_VALUE 127
#define CHANNEL_1 0
#define CHANNEL_2 1
#define CHANNEL_3 2

#define TXT_CONNECTED         "Connected"
#define TXT_DISCONNECTED      "Disconnected"
#define TXT_NOT_CONNECTED     "Device not connected"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

public slots:
  void hid_comm_update(int version,bool isConnected, int chv1, int chv2, int chv3, int potentiometerValue);

signals:
  void update_channels_hid(int v1,int v2,int v3);
  void read_levels();
  void reset_to_bl();


private slots:

  void on_pbDefault_clicked();

  void on_tbCh1Up_clicked();

  void on_tbCh1Down_clicked();

  void on_tbCh2Up_clicked();

  void on_tbCh2Down_clicked();

  void on_tbCh3Up_clicked();

  void on_tbCh3Down_clicked();

  void on_pbFirmwareUpdate_clicked();

  void on_pbRead_clicked();

  void on_leCh1Value_editingFinished();

  void on_leCh2Value_editingFinished();

  void on_leCh3Value_editingFinished();

private:

  void enable_widgets(bool enable);
  void update_chanels(uint8_t *values);
  void set_temperature( double temp  );

  void increment_level(uint8_t no );
  void decrement_level(uint8_t no );

  Ui::MainWindow
    *ui;

  QLabel
    *lblConStatus,
    *lblFirmwareVersion;

  uint8_t
   level_values_[3];


  UpdateWindow
    *updtform_;

  HID_PnP
    *plugNPlay;

};

#endif // MAINWINDOW_H
