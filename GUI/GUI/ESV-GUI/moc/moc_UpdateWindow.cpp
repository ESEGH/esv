/****************************************************************************
** Meta object code from reading C++ file 'UpdateWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../UpdateWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'UpdateWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_UpdateWindow_t {
    QByteArrayData data[27];
    char stringdata0[514];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_UpdateWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_UpdateWindow_t qt_meta_stringdata_UpdateWindow = {
    {
QT_MOC_LITERAL(0, 0, 12), // "UpdateWindow"
QT_MOC_LITERAL(1, 13, 21), // "IoWithDeviceCompleted"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 3), // "msg"
QT_MOC_LITERAL(4, 40, 15), // "Comm::ErrorCode"
QT_MOC_LITERAL(5, 56, 4), // "time"
QT_MOC_LITERAL(6, 61, 19), // "IoWithDeviceStarted"
QT_MOC_LITERAL(7, 81, 12), // "AppendString"
QT_MOC_LITERAL(8, 94, 11), // "ShowMessage"
QT_MOC_LITERAL(9, 106, 14), // "SetProgressBar"
QT_MOC_LITERAL(10, 121, 8), // "newValue"
QT_MOC_LITERAL(11, 130, 10), // "Connection"
QT_MOC_LITERAL(12, 141, 14), // "openRecentFile"
QT_MOC_LITERAL(13, 156, 20), // "IoWithDeviceComplete"
QT_MOC_LITERAL(14, 177, 17), // "IoWithDeviceStart"
QT_MOC_LITERAL(15, 195, 21), // "AppendStringToTextbox"
QT_MOC_LITERAL(16, 217, 14), // "ShowMessageBox"
QT_MOC_LITERAL(17, 232, 17), // "UpdateProgressBar"
QT_MOC_LITERAL(18, 250, 30), // "on_actionBlank_Check_triggered"
QT_MOC_LITERAL(19, 281, 31), // "on_actionReset_Device_triggered"
QT_MOC_LITERAL(20, 313, 28), // "on_action_Settings_triggered"
QT_MOC_LITERAL(21, 342, 33), // "on_action_Verify_Device_trigg..."
QT_MOC_LITERAL(22, 376, 25), // "on_action_About_triggered"
QT_MOC_LITERAL(23, 402, 31), // "on_actionWrite_Device_triggered"
QT_MOC_LITERAL(24, 434, 23), // "on_actionOpen_triggered"
QT_MOC_LITERAL(25, 458, 31), // "on_actionErase_Device_triggered"
QT_MOC_LITERAL(26, 490, 23) // "on_actionExit_triggered"

    },
    "UpdateWindow\0IoWithDeviceCompleted\0\0"
    "msg\0Comm::ErrorCode\0time\0IoWithDeviceStarted\0"
    "AppendString\0ShowMessage\0SetProgressBar\0"
    "newValue\0Connection\0openRecentFile\0"
    "IoWithDeviceComplete\0IoWithDeviceStart\0"
    "AppendStringToTextbox\0ShowMessageBox\0"
    "UpdateProgressBar\0on_actionBlank_Check_triggered\0"
    "on_actionReset_Device_triggered\0"
    "on_action_Settings_triggered\0"
    "on_action_Verify_Device_triggered\0"
    "on_action_About_triggered\0"
    "on_actionWrite_Device_triggered\0"
    "on_actionOpen_triggered\0"
    "on_actionErase_Device_triggered\0"
    "on_actionExit_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_UpdateWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,  119,    2, 0x06 /* Public */,
       6,    1,  126,    2, 0x06 /* Public */,
       7,    1,  129,    2, 0x06 /* Public */,
       8,    1,  132,    2, 0x06 /* Public */,
       9,    1,  135,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,  138,    2, 0x0a /* Public */,
      12,    0,  139,    2, 0x0a /* Public */,
      13,    3,  140,    2, 0x0a /* Public */,
      14,    1,  147,    2, 0x0a /* Public */,
      15,    1,  150,    2, 0x0a /* Public */,
      16,    1,  153,    2, 0x0a /* Public */,
      17,    1,  156,    2, 0x0a /* Public */,
      18,    0,  159,    2, 0x08 /* Private */,
      19,    0,  160,    2, 0x08 /* Private */,
      20,    0,  161,    2, 0x08 /* Private */,
      21,    0,  162,    2, 0x08 /* Private */,
      22,    0,  163,    2, 0x08 /* Private */,
      23,    0,  164,    2, 0x08 /* Private */,
      24,    0,  165,    2, 0x08 /* Private */,
      25,    0,  166,    2, 0x08 /* Private */,
      26,    0,  167,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4, QMetaType::Double,    3,    2,    5,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Int,   10,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4, QMetaType::Double,    3,    2,    5,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void UpdateWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        UpdateWindow *_t = static_cast<UpdateWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->IoWithDeviceCompleted((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< Comm::ErrorCode(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        case 1: _t->IoWithDeviceStarted((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->AppendString((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->ShowMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->SetProgressBar((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->Connection(); break;
        case 6: _t->openRecentFile(); break;
        case 7: _t->IoWithDeviceComplete((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< Comm::ErrorCode(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        case 8: _t->IoWithDeviceStart((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->AppendStringToTextbox((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->ShowMessageBox((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->UpdateProgressBar((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_actionBlank_Check_triggered(); break;
        case 13: _t->on_actionReset_Device_triggered(); break;
        case 14: _t->on_action_Settings_triggered(); break;
        case 15: _t->on_action_Verify_Device_triggered(); break;
        case 16: _t->on_action_About_triggered(); break;
        case 17: _t->on_actionWrite_Device_triggered(); break;
        case 18: _t->on_actionOpen_triggered(); break;
        case 19: _t->on_actionErase_Device_triggered(); break;
        case 20: _t->on_actionExit_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (UpdateWindow::*_t)(QString , Comm::ErrorCode , double );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&UpdateWindow::IoWithDeviceCompleted)) {
                *result = 0;
            }
        }
        {
            typedef void (UpdateWindow::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&UpdateWindow::IoWithDeviceStarted)) {
                *result = 1;
            }
        }
        {
            typedef void (UpdateWindow::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&UpdateWindow::AppendString)) {
                *result = 2;
            }
        }
        {
            typedef void (UpdateWindow::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&UpdateWindow::ShowMessage)) {
                *result = 3;
            }
        }
        {
            typedef void (UpdateWindow::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&UpdateWindow::SetProgressBar)) {
                *result = 4;
            }
        }
    }
}

const QMetaObject UpdateWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_UpdateWindow.data,
      qt_meta_data_UpdateWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *UpdateWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *UpdateWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_UpdateWindow.stringdata0))
        return static_cast<void*>(const_cast< UpdateWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int UpdateWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void UpdateWindow::IoWithDeviceCompleted(QString _t1, Comm::ErrorCode _t2, double _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void UpdateWindow::IoWithDeviceStarted(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void UpdateWindow::AppendString(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void UpdateWindow::ShowMessage(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void UpdateWindow::SetProgressBar(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
