/****************************************************************************
** Meta object code from reading C++ file 'hid_pnp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../hid_pnp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'hid_pnp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_HID_PnP_t {
    QByteArrayData data[16];
    char stringdata0[136];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_HID_PnP_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_HID_PnP_t qt_meta_stringdata_HID_PnP = {
    {
QT_MOC_LITERAL(0, 0, 7), // "HID_PnP"
QT_MOC_LITERAL(1, 8, 15), // "hid_comm_update"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 7), // "version"
QT_MOC_LITERAL(4, 33, 11), // "isConnected"
QT_MOC_LITERAL(5, 45, 4), // "chv1"
QT_MOC_LITERAL(6, 50, 4), // "chv2"
QT_MOC_LITERAL(7, 55, 4), // "chv3"
QT_MOC_LITERAL(8, 60, 18), // "potentiometerValue"
QT_MOC_LITERAL(9, 79, 11), // "read_levels"
QT_MOC_LITERAL(10, 91, 7), // "PollUSB"
QT_MOC_LITERAL(11, 99, 15), // "update_channels"
QT_MOC_LITERAL(12, 115, 2), // "v1"
QT_MOC_LITERAL(13, 118, 2), // "v2"
QT_MOC_LITERAL(14, 121, 2), // "v3"
QT_MOC_LITERAL(15, 124, 11) // "reset_to_bl"

    },
    "HID_PnP\0hid_comm_update\0\0version\0"
    "isConnected\0chv1\0chv2\0chv3\0"
    "potentiometerValue\0read_levels\0PollUSB\0"
    "update_channels\0v1\0v2\0v3\0reset_to_bl"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_HID_PnP[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    6,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    0,   52,    2, 0x0a /* Public */,
      10,    0,   53,    2, 0x0a /* Public */,
      11,    3,   54,    2, 0x0a /* Public */,
      15,    0,   61,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Bool, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,    3,    4,    5,    6,    7,    8,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,   12,   13,   14,
    QMetaType::Void,

       0        // eod
};

void HID_PnP::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        HID_PnP *_t = static_cast<HID_PnP *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->hid_comm_update((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 1: _t->read_levels(); break;
        case 2: _t->PollUSB(); break;
        case 3: _t->update_channels((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->reset_to_bl(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (HID_PnP::*_t)(int , bool , int , int , int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&HID_PnP::hid_comm_update)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject HID_PnP::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_HID_PnP.data,
      qt_meta_data_HID_PnP,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *HID_PnP::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *HID_PnP::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_HID_PnP.stringdata0))
        return static_cast<void*>(const_cast< HID_PnP*>(this));
    return QObject::qt_metacast(_clname);
}

int HID_PnP::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void HID_PnP::hid_comm_update(int _t1, bool _t2, int _t3, int _t4, int _t5, int _t6)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
