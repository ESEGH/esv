/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[27];
    char stringdata0[412];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 19), // "update_channels_hid"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 2), // "v1"
QT_MOC_LITERAL(4, 35, 2), // "v2"
QT_MOC_LITERAL(5, 38, 2), // "v3"
QT_MOC_LITERAL(6, 41, 11), // "read_levels"
QT_MOC_LITERAL(7, 53, 11), // "reset_to_bl"
QT_MOC_LITERAL(8, 65, 15), // "hid_comm_update"
QT_MOC_LITERAL(9, 81, 7), // "version"
QT_MOC_LITERAL(10, 89, 11), // "isConnected"
QT_MOC_LITERAL(11, 101, 4), // "chv1"
QT_MOC_LITERAL(12, 106, 4), // "chv2"
QT_MOC_LITERAL(13, 111, 4), // "chv3"
QT_MOC_LITERAL(14, 116, 18), // "potentiometerValue"
QT_MOC_LITERAL(15, 135, 20), // "on_pbDefault_clicked"
QT_MOC_LITERAL(16, 156, 18), // "on_tbCh1Up_clicked"
QT_MOC_LITERAL(17, 175, 20), // "on_tbCh1Down_clicked"
QT_MOC_LITERAL(18, 196, 18), // "on_tbCh2Up_clicked"
QT_MOC_LITERAL(19, 215, 20), // "on_tbCh2Down_clicked"
QT_MOC_LITERAL(20, 236, 18), // "on_tbCh3Up_clicked"
QT_MOC_LITERAL(21, 255, 20), // "on_tbCh3Down_clicked"
QT_MOC_LITERAL(22, 276, 27), // "on_pbFirmwareUpdate_clicked"
QT_MOC_LITERAL(23, 304, 17), // "on_pbRead_clicked"
QT_MOC_LITERAL(24, 322, 29), // "on_leCh1Value_editingFinished"
QT_MOC_LITERAL(25, 352, 29), // "on_leCh2Value_editingFinished"
QT_MOC_LITERAL(26, 382, 29) // "on_leCh3Value_editingFinished"

    },
    "MainWindow\0update_channels_hid\0\0v1\0"
    "v2\0v3\0read_levels\0reset_to_bl\0"
    "hid_comm_update\0version\0isConnected\0"
    "chv1\0chv2\0chv3\0potentiometerValue\0"
    "on_pbDefault_clicked\0on_tbCh1Up_clicked\0"
    "on_tbCh1Down_clicked\0on_tbCh2Up_clicked\0"
    "on_tbCh2Down_clicked\0on_tbCh3Up_clicked\0"
    "on_tbCh3Down_clicked\0on_pbFirmwareUpdate_clicked\0"
    "on_pbRead_clicked\0on_leCh1Value_editingFinished\0"
    "on_leCh2Value_editingFinished\0"
    "on_leCh3Value_editingFinished"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   94,    2, 0x06 /* Public */,
       6,    0,  101,    2, 0x06 /* Public */,
       7,    0,  102,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    6,  103,    2, 0x0a /* Public */,
      15,    0,  116,    2, 0x08 /* Private */,
      16,    0,  117,    2, 0x08 /* Private */,
      17,    0,  118,    2, 0x08 /* Private */,
      18,    0,  119,    2, 0x08 /* Private */,
      19,    0,  120,    2, 0x08 /* Private */,
      20,    0,  121,    2, 0x08 /* Private */,
      21,    0,  122,    2, 0x08 /* Private */,
      22,    0,  123,    2, 0x08 /* Private */,
      23,    0,  124,    2, 0x08 /* Private */,
      24,    0,  125,    2, 0x08 /* Private */,
      25,    0,  126,    2, 0x08 /* Private */,
      26,    0,  127,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,    3,    4,    5,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Bool, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,    9,   10,   11,   12,   13,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_channels_hid((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->read_levels(); break;
        case 2: _t->reset_to_bl(); break;
        case 3: _t->hid_comm_update((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 4: _t->on_pbDefault_clicked(); break;
        case 5: _t->on_tbCh1Up_clicked(); break;
        case 6: _t->on_tbCh1Down_clicked(); break;
        case 7: _t->on_tbCh2Up_clicked(); break;
        case 8: _t->on_tbCh2Down_clicked(); break;
        case 9: _t->on_tbCh3Up_clicked(); break;
        case 10: _t->on_tbCh3Down_clicked(); break;
        case 11: _t->on_pbFirmwareUpdate_clicked(); break;
        case 12: _t->on_pbRead_clicked(); break;
        case 13: _t->on_leCh1Value_editingFinished(); break;
        case 14: _t->on_leCh2Value_editingFinished(); break;
        case 15: _t->on_leCh3Value_editingFinished(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)(int , int , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::update_channels_hid)) {
                *result = 0;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::read_levels)) {
                *result = 1;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::reset_to_bl)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::update_channels_hid(int _t1, int _t2, int _t3)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::read_levels()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void MainWindow::reset_to_bl()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
