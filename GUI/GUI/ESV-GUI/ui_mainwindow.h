/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayoutLogo;
    QSpacerItem *horizontalSpacer_14;
    QLabel *lblCalText;
    QSpacerItem *horizontalSpacer;
    QLabel *lblLogo;
    QSpacerItem *verticalSpacerLogoControls;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayoutPegelText;
    QLabel *lblCalText1;
    QLabel *lblCalText2;
    QLabel *lblCalText3;
    QSpacerItem *horizontalSpacerTextCh1;
    QGroupBox *gpChannel1;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayoutCh1Up;
    QToolButton *tbCh1Up;
    QLineEdit *leCh1Value;
    QHBoxLayout *horizontalLayoutCh1Down;
    QToolButton *tbCh1Down;
    QSpacerItem *horizontalSpacerCh1Ch2;
    QGroupBox *gpChannel2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayoutCh2Up;
    QToolButton *tbCh2Up;
    QLineEdit *leCh2Value;
    QHBoxLayout *horizontalLayoutCh2Down;
    QToolButton *tbCh2Down;
    QSpacerItem *horizontalSpacerCh2Ch3;
    QGroupBox *gpChannel3;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayoutCh3Up;
    QToolButton *tbCh3Up;
    QLineEdit *leCh3Value;
    QHBoxLayout *horizontalLayoutCh3Down;
    QToolButton *tbCh3Down;
    QSpacerItem *horizontalSpacerCh3Temp;
    QGroupBox *gpTemerature;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_3;
    QLineEdit *leTemperature;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacerControlsBtns;
    QHBoxLayout *horizontalLayoutBtns;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pbDefault;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pbFirmwareUpdate;
    QSpacerItem *horizontalSpacerBtn3Right;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(720, 400);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(720, 400));
        MainWindow->setMaximumSize(QSize(720, 400));
        QFont font;
        font.setFamily(QStringLiteral("Tahoma"));
        MainWindow->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/images/esv.ico"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_5 = new QVBoxLayout(centralWidget);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayoutLogo = new QHBoxLayout();
        horizontalLayoutLogo->setSpacing(9);
        horizontalLayoutLogo->setObjectName(QStringLiteral("horizontalLayoutLogo"));
        horizontalLayoutLogo->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayoutLogo->addItem(horizontalSpacer_14);

        lblCalText = new QLabel(centralWidget);
        lblCalText->setObjectName(QStringLiteral("lblCalText"));
        QFont font1;
        font1.setFamily(QStringLiteral("Tahoma"));
        font1.setPointSize(14);
        lblCalText->setFont(font1);
        lblCalText->setAlignment(Qt::AlignCenter);

        horizontalLayoutLogo->addWidget(lblCalText);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayoutLogo->addItem(horizontalSpacer);

        lblLogo = new QLabel(centralWidget);
        lblLogo->setObjectName(QStringLiteral("lblLogo"));
        lblLogo->setPixmap(QPixmap(QString::fromUtf8(":/images/logo.png")));
        lblLogo->setScaledContents(false);
        lblLogo->setAlignment(Qt::AlignCenter);

        horizontalLayoutLogo->addWidget(lblLogo);


        verticalLayout_5->addLayout(horizontalLayoutLogo);

        verticalSpacerLogoControls = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacerLogoControls);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayoutPegelText = new QVBoxLayout();
        verticalLayoutPegelText->setSpacing(6);
        verticalLayoutPegelText->setObjectName(QStringLiteral("verticalLayoutPegelText"));
        lblCalText1 = new QLabel(centralWidget);
        lblCalText1->setObjectName(QStringLiteral("lblCalText1"));
        QFont font2;
        font2.setFamily(QStringLiteral("Tahoma"));
        font2.setPointSize(12);
        lblCalText1->setFont(font2);
        lblCalText1->setAlignment(Qt::AlignCenter);

        verticalLayoutPegelText->addWidget(lblCalText1);

        lblCalText2 = new QLabel(centralWidget);
        lblCalText2->setObjectName(QStringLiteral("lblCalText2"));
        lblCalText2->setFont(font2);
        lblCalText2->setAlignment(Qt::AlignCenter);

        verticalLayoutPegelText->addWidget(lblCalText2);

        lblCalText3 = new QLabel(centralWidget);
        lblCalText3->setObjectName(QStringLiteral("lblCalText3"));
        lblCalText3->setFont(font2);
        lblCalText3->setAlignment(Qt::AlignCenter);

        verticalLayoutPegelText->addWidget(lblCalText3);


        horizontalLayout_2->addLayout(verticalLayoutPegelText);

        horizontalSpacerTextCh1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacerTextCh1);

        gpChannel1 = new QGroupBox(centralWidget);
        gpChannel1->setObjectName(QStringLiteral("gpChannel1"));
        gpChannel1->setEnabled(false);
        gpChannel1->setFont(font2);
        gpChannel1->setAlignment(Qt::AlignCenter);
        verticalLayout = new QVBoxLayout(gpChannel1);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(-1, 5, -1, -1);
        horizontalLayoutCh1Up = new QHBoxLayout();
        horizontalLayoutCh1Up->setSpacing(6);
        horizontalLayoutCh1Up->setObjectName(QStringLiteral("horizontalLayoutCh1Up"));
        tbCh1Up = new QToolButton(gpChannel1);
        tbCh1Up->setObjectName(QStringLiteral("tbCh1Up"));
        tbCh1Up->setMinimumSize(QSize(50, 50));
        tbCh1Up->setMaximumSize(QSize(0, 16777215));
        tbCh1Up->setArrowType(Qt::UpArrow);

        horizontalLayoutCh1Up->addWidget(tbCh1Up);


        verticalLayout->addLayout(horizontalLayoutCh1Up);

        leCh1Value = new QLineEdit(gpChannel1);
        leCh1Value->setObjectName(QStringLiteral("leCh1Value"));
        leCh1Value->setMinimumSize(QSize(0, 25));
        leCh1Value->setMaximumSize(QSize(100, 16777215));
        QFont font3;
        font3.setPointSize(12);
        leCh1Value->setFont(font3);
        leCh1Value->setFocusPolicy(Qt::StrongFocus);
        leCh1Value->setInputMask(QStringLiteral(""));
        leCh1Value->setCursorPosition(3);
        leCh1Value->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(leCh1Value);

        horizontalLayoutCh1Down = new QHBoxLayout();
        horizontalLayoutCh1Down->setSpacing(6);
        horizontalLayoutCh1Down->setObjectName(QStringLiteral("horizontalLayoutCh1Down"));
        tbCh1Down = new QToolButton(gpChannel1);
        tbCh1Down->setObjectName(QStringLiteral("tbCh1Down"));
        tbCh1Down->setMinimumSize(QSize(50, 50));
        tbCh1Down->setArrowType(Qt::DownArrow);

        horizontalLayoutCh1Down->addWidget(tbCh1Down);


        verticalLayout->addLayout(horizontalLayoutCh1Down);


        horizontalLayout_2->addWidget(gpChannel1);

        horizontalSpacerCh1Ch2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacerCh1Ch2);

        gpChannel2 = new QGroupBox(centralWidget);
        gpChannel2->setObjectName(QStringLiteral("gpChannel2"));
        gpChannel2->setEnabled(false);
        gpChannel2->setFont(font2);
        gpChannel2->setAlignment(Qt::AlignCenter);
        verticalLayout_2 = new QVBoxLayout(gpChannel2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, 5, -1, -1);
        horizontalLayoutCh2Up = new QHBoxLayout();
        horizontalLayoutCh2Up->setSpacing(6);
        horizontalLayoutCh2Up->setObjectName(QStringLiteral("horizontalLayoutCh2Up"));
        tbCh2Up = new QToolButton(gpChannel2);
        tbCh2Up->setObjectName(QStringLiteral("tbCh2Up"));
        tbCh2Up->setMinimumSize(QSize(50, 50));
        tbCh2Up->setMaximumSize(QSize(0, 16777215));
        tbCh2Up->setArrowType(Qt::UpArrow);

        horizontalLayoutCh2Up->addWidget(tbCh2Up);


        verticalLayout_2->addLayout(horizontalLayoutCh2Up);

        leCh2Value = new QLineEdit(gpChannel2);
        leCh2Value->setObjectName(QStringLiteral("leCh2Value"));
        leCh2Value->setMinimumSize(QSize(0, 25));
        leCh2Value->setMaximumSize(QSize(100, 16777215));
        leCh2Value->setFont(font3);
        leCh2Value->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(leCh2Value);

        horizontalLayoutCh2Down = new QHBoxLayout();
        horizontalLayoutCh2Down->setSpacing(6);
        horizontalLayoutCh2Down->setObjectName(QStringLiteral("horizontalLayoutCh2Down"));
        tbCh2Down = new QToolButton(gpChannel2);
        tbCh2Down->setObjectName(QStringLiteral("tbCh2Down"));
        tbCh2Down->setMinimumSize(QSize(50, 50));
        tbCh2Down->setArrowType(Qt::DownArrow);

        horizontalLayoutCh2Down->addWidget(tbCh2Down);


        verticalLayout_2->addLayout(horizontalLayoutCh2Down);


        horizontalLayout_2->addWidget(gpChannel2);

        horizontalSpacerCh2Ch3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacerCh2Ch3);

        gpChannel3 = new QGroupBox(centralWidget);
        gpChannel3->setObjectName(QStringLiteral("gpChannel3"));
        gpChannel3->setEnabled(false);
        gpChannel3->setFont(font2);
        gpChannel3->setAlignment(Qt::AlignCenter);
        verticalLayout_3 = new QVBoxLayout(gpChannel3);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(-1, 5, -1, -1);
        horizontalLayoutCh3Up = new QHBoxLayout();
        horizontalLayoutCh3Up->setSpacing(6);
        horizontalLayoutCh3Up->setObjectName(QStringLiteral("horizontalLayoutCh3Up"));
        tbCh3Up = new QToolButton(gpChannel3);
        tbCh3Up->setObjectName(QStringLiteral("tbCh3Up"));
        tbCh3Up->setMinimumSize(QSize(50, 50));
        tbCh3Up->setMaximumSize(QSize(0, 16777215));
        tbCh3Up->setArrowType(Qt::UpArrow);

        horizontalLayoutCh3Up->addWidget(tbCh3Up);


        verticalLayout_3->addLayout(horizontalLayoutCh3Up);

        leCh3Value = new QLineEdit(gpChannel3);
        leCh3Value->setObjectName(QStringLiteral("leCh3Value"));
        leCh3Value->setMinimumSize(QSize(0, 25));
        leCh3Value->setMaximumSize(QSize(100, 16777215));
        leCh3Value->setFont(font3);
        leCh3Value->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(leCh3Value);

        horizontalLayoutCh3Down = new QHBoxLayout();
        horizontalLayoutCh3Down->setSpacing(6);
        horizontalLayoutCh3Down->setObjectName(QStringLiteral("horizontalLayoutCh3Down"));
        tbCh3Down = new QToolButton(gpChannel3);
        tbCh3Down->setObjectName(QStringLiteral("tbCh3Down"));
        tbCh3Down->setMinimumSize(QSize(50, 50));
        tbCh3Down->setArrowType(Qt::DownArrow);

        horizontalLayoutCh3Down->addWidget(tbCh3Down);


        verticalLayout_3->addLayout(horizontalLayoutCh3Down);


        horizontalLayout_2->addWidget(gpChannel3);

        horizontalSpacerCh3Temp = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacerCh3Temp);

        gpTemerature = new QGroupBox(centralWidget);
        gpTemerature->setObjectName(QStringLiteral("gpTemerature"));
        gpTemerature->setEnabled(false);
        gpTemerature->setFont(font2);
        gpTemerature->setAlignment(Qt::AlignCenter);
        verticalLayout_4 = new QVBoxLayout(gpTemerature);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_3 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        leTemperature = new QLineEdit(gpTemerature);
        leTemperature->setObjectName(QStringLiteral("leTemperature"));
        leTemperature->setMinimumSize(QSize(0, 25));
        leTemperature->setMaximumSize(QSize(100, 16777215));
        leTemperature->setFont(font3);
        leTemperature->setAlignment(Qt::AlignCenter);
        leTemperature->setReadOnly(true);

        horizontalLayout->addWidget(leTemperature);

        horizontalSpacer_2 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_4->addLayout(horizontalLayout);


        horizontalLayout_2->addWidget(gpTemerature);


        verticalLayout_5->addLayout(horizontalLayout_2);

        verticalSpacerControlsBtns = new QSpacerItem(20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacerControlsBtns);

        horizontalLayoutBtns = new QHBoxLayout();
        horizontalLayoutBtns->setSpacing(6);
        horizontalLayoutBtns->setObjectName(QStringLiteral("horizontalLayoutBtns"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayoutBtns->addItem(horizontalSpacer_5);

        pbDefault = new QPushButton(centralWidget);
        pbDefault->setObjectName(QStringLiteral("pbDefault"));
        pbDefault->setEnabled(false);
        pbDefault->setMinimumSize(QSize(110, 30));
        QFont font4;
        font4.setFamily(QStringLiteral("Tahoma"));
        font4.setPointSize(10);
        pbDefault->setFont(font4);

        horizontalLayoutBtns->addWidget(pbDefault);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayoutBtns->addItem(horizontalSpacer_4);

        pbFirmwareUpdate = new QPushButton(centralWidget);
        pbFirmwareUpdate->setObjectName(QStringLiteral("pbFirmwareUpdate"));
        pbFirmwareUpdate->setEnabled(true);
        pbFirmwareUpdate->setMinimumSize(QSize(110, 30));
        QFont font5;
        font5.setPointSize(10);
        pbFirmwareUpdate->setFont(font5);

        horizontalLayoutBtns->addWidget(pbFirmwareUpdate);

        horizontalSpacerBtn3Right = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayoutBtns->addItem(horizontalSpacerBtn3Right);


        verticalLayout_5->addLayout(horizontalLayoutBtns);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "ESV calibration tool ???", 0));
        lblCalText->setText(QApplication::translate("MainWindow", "ESV calibration", 0));
        lblLogo->setText(QString());
        lblCalText1->setText(QApplication::translate("MainWindow", "UP", 0));
        lblCalText2->setText(QApplication::translate("MainWindow", " Current level ", 0));
        lblCalText3->setText(QApplication::translate("MainWindow", "DOWN", 0));
        gpChannel1->setTitle(QApplication::translate("MainWindow", "Channel 1", 0));
        tbCh1Up->setText(QApplication::translate("MainWindow", "...", 0));
        leCh1Value->setText(QApplication::translate("MainWindow", "127", 0));
        tbCh1Down->setText(QApplication::translate("MainWindow", "...", 0));
        gpChannel2->setTitle(QApplication::translate("MainWindow", "Channel 2", 0));
        tbCh2Up->setText(QApplication::translate("MainWindow", "...", 0));
        leCh2Value->setText(QApplication::translate("MainWindow", "127", 0));
        tbCh2Down->setText(QApplication::translate("MainWindow", "...", 0));
        gpChannel3->setTitle(QApplication::translate("MainWindow", "Channel 3", 0));
        tbCh3Up->setText(QApplication::translate("MainWindow", "...", 0));
        leCh3Value->setText(QApplication::translate("MainWindow", "127", 0));
        tbCh3Down->setText(QApplication::translate("MainWindow", "...", 0));
        gpTemerature->setTitle(QApplication::translate("MainWindow", "Temperature", 0));
        leTemperature->setText(QApplication::translate("MainWindow", "20\302\260 C", 0));
        pbDefault->setText(QApplication::translate("MainWindow", "Default values", 0));
        pbFirmwareUpdate->setText(QApplication::translate("MainWindow", "Firmware update", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
