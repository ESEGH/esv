# -------------------------------------------------
# Project created by QtCreator 2010-10-29T15:54:07
# -------------------------------------------------
QT -= gui
TARGET = HIDAPI_BL
TEMPLATE = lib
CONFIG += staticlib
HEADERS += hidapi_bl.h
SOURCES += hid_bl.cpp
OBJECTS_DIR = obj
MOC_DIR     = moc
